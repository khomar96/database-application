
package exo2;

import exo2.Livre;
import javax.ejb.Stateless;
import javax.ejb.Startup;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

 
@Stateless


public class GestionLivreStateless implements GestionLivre{

/*public void nouveauLivre(String titre, String isbn){

	void nouveauLivre(String isbn,String titre);
	
	List<Book> getLivres();
	*/
	
	public GestionLivreStateless() {
		//Constructeur par d�faut
	}
	
	@PersistenceContext(unitName="livre")
	//permet de g�rer un groupe d'entit�s qui maintient les donn�e dans un 
	// "persisatence store" comme les bases de donn�es
	
	// Pour chaque identifiant d'entit� , il y a une unique entit� d'instance
	private EntityManager entityManager;
	//EntityManager: interface utilis�e dans un contexte de persistence
	//Il est utilis� pour trouver les entit�s avec leur clefs primaires et de g�rer 
	//les requ�tes avec les entit�s
	
	public void nouveauLivre(String titre,String isbn) {
		if( entityManager.find(Livre.class,isbn)==null) {
			Livre newLivre=new Livre( titre, isbn);
			System.out.println("apres new livre");
			entityManager.persist(newLivre);//Stocker dans la base de donne�e
			entityManager.merge(newLivre);//Methode supprimer de l'entity manager
		}
	}
	
	public void supprimerLivre(String isbn) {
		
		Livre livreAsupprimer=entityManager.find(Livre.class, isbn);
		entityManager.remove(livreAsupprimer);//Methode supprimer de l'entity manager
		entityManager.merge(livreAsupprimer);//Methode supprimer de l'entity manager

	}
	
	public Livre retrouverLivre(String isbn) {
		
		return entityManager.find(Livre.class,isbn);
		
	}
	
	public void empreinterLivre(String isbn) {
		Livre livreAempreinter=entityManager.find(Livre.class, isbn);
		
		if(livreAempreinter!=null) {
		if (livreAempreinter.getDispo()==1  ){
			livreAempreinter.setDispo(0);
			entityManager.merge(livreAempreinter);//Methode supprimer de l'entity manager

		
		}
		}
	}
	public List<Livre> getLivres(){
		
		return entityManager.createQuery("From livres").getResultList();
		
	}
	
	public void rendreLivre(String isbn) {
		Livre livreArendre=entityManager.find(Livre.class, isbn);
		
		if (livreArendre.getDispo()==0) {
			livreArendre.setDispo(1);
			entityManager.merge(livreArendre);//Methode supprimer de l'entity manager

		}


		
	}
	
	
}


