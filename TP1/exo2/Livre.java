
package exo2;

import javax.naming.*;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="livre")
public class Livre implements Serializable{
	
	@Id 
	private String isbn;
	private String titre;
	private int dispo;

Livre(String titre,String isbn)
{
	this.titre=titre;
	this.isbn=isbn;
	this.dispo=1;
}

Livre(){//Constructeur par d�faut
	
	
}

void setDispo(int disp) {
	this.dispo=disp;
}

int getDispo() {
	
	return dispo;
}


String getTitre() {
	return titre;
}

}

