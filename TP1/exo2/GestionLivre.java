
package exo2;
//import exo2.Livre;
import java.util.List;
import javax.ejb.Remote;

@Remote


public interface GestionLivre{


	void nouveauLivre(String isbn,String titre);
	void supprimerLivre(String isbn);//Supprimer un livre avec son isbn comme param�tre
	Livre retrouverLivre(String isbn);
	void empreinterLivre(String isbn);
	List<Livre> getLivres();
	void rendreLivre(String isbn);
	
	
}


